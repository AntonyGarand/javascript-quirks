# Challenge

While we're glad ES7 brings us `async` and `await`, asynchronous code still isn't as straightforward as it could be.
Try guessing what the following snippet should return, then head up to the writeup!

```javascript
function sleepOneSecondAndReturnTwo() {
    return new Promise(resolve =>  {
        setTimeout(() => { resolve(2); }, 1000);
    });
}

let x = 0;

async function incrementXInOneSecond() {
    x += await sleepOneSecondAndReturnTwo();
    console.log(x);
}

incrementXInOneSecond();
x++;
console.log(x);
```

This can be simplified quite a bit due to how asynchronous code is handled within JavaScript.

The `setTimeout` and creation of a new function is not necessary, as the asynchronous part of the execution will be delayed even there is no delay in the promise resolution.
`await` will also convert non-promises to resolved promise, as described on [MDN's await page](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/await)

> If the value of the expression following the await operator is not a Promise, it's converted to a resolved Promise.

`await 2` is therefore the shorthand syntax of `await Promise.resolve(2);`.

This leads us to the following code:

```javascript
let x = 0;

async function incrementX() {
    x += await 2;
    console.log(x);
}

incrementX();
x++;
console.log(x);
```
