# Challenge
There are three difficulty levels for this challenge, and many available solutions!  
Your objective is to give X the required value for `Flag!` to be printed.

Place the following snippet somewhere in order to print `Flag!`  
You do control the scope of the if statement.
## Level 1
```javascript
// Your code here

if(x == 1 && x == 2 && x == 3) {
    console.log('Flag!');
} else {
    console.log('Wrong flag!');
}

// Your code here
```

## Level 2
Let's make things a bit harder by using the strict equal operator!
```javascript
// Your code here

if(x === 1 && x === 2 && x === 3) {
    console.log('Flag!');
} else {
    console.log('Wrong flag!');
}

// Your code here
```

## Level 3
Finally, let's print the flag within the current scope!  
This means this statement should not be within a class or function, but by itself within the script.
```javascript
// Your code here

if(x === 1 && x === 2 && x === 3) {
    console.log('Flag!');
} else {
    console.log('Wrong flag!');
}
```
