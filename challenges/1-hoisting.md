Replace `// Your code here` with your code, and print `Flag!`!

```javascript
function generateSecret() {
  return Date.now() + Math.random() * 10000;
}

const mySecretKey = generateSecret();

// Your code here

if (mySecretKey === 42) {
    console.log('Flag!');
} else {
    console.log('Bad secret!');
}
```